#pragma once
#include "Money.h"
#include <string>
#include <iostream>
#include <fstream>
class Goods {
	std::string _title{};
	long int _quantity{};
	unsigned long int _weight{};
	unsigned long int _shelfLife{};
	Money _price{};
public:
	Goods();
	Goods(const Goods& obj);
	const std::string getTitle() const;
	const unsigned long int getQuantity() const;
	const unsigned long int getWeight() const;
	const unsigned long int getShelfLife() const;
	const Money getPrice() const;
	void setTitle(std::string str);
	void setQuantity(const unsigned long int value);
	void setWeight(const unsigned long int value);
	void setShelfLife(const unsigned long int value);
	void setPrice(const Money& price);
};
std::ostream& operator<<(std::ostream& out, const Goods& obj);
std::fstream& printNote(std::fstream& out, Goods& obj);