#include <iostream>
#include "main.h"
#include "FuncIni.h"
#include <string>

using namespace std;

void main()
{
	setlocale(LC_ALL, "Russian");
	
	std::fstream file;
	//in.close();
	//std::ofstream out(GOODS_SHEET);
	//out.close();
	bool trigger{}, wrong{};
	int id{};
	string buffer_login{};
	string buffer_password{};
	//�����������
	while (!trigger) {
		system("cls");
		if (wrong) {
			cout << "�������� ����� ��� ������\n\n";
		}
		cout << "Login: ";
		cin >> buffer_login;
		cout << "Password: ";
		cin >> buffer_password;
		if ((buffer_login == SALE_LOGIN) && (buffer_password == SALE_PASSWORD)) {
			trigger = true;
			id = 1;
		}
		else if ((buffer_login == PURCHASE_LOGIN) && (buffer_password == PURCHASE_PASSWORD)) {
			trigger = true;
			id = 2;
		}
		else {
			wrong = true;
		}
	}
	system("cls");
	//����������
	if (id == 1) {
		cout << "\n����� ������\n\n";
		bool trigger = false;
		int task{};
		do {
			cout << '\n';
			menuSale(task);
			switch (task)
			{
			case 1:
				system("cls");
				file.open(GOODS_SHEET);
				readSheet(file);
				file.close();
				break;
			case 2:
				system("cls");
				file.open(GOODS_SHEET, std::ofstream::out | ios::app);
				addNote(file);
				file.close();
				break;
			case 3:
				system("cls");
				file.open(GOODS_SHEET);
				editNote(file);
				file.close();
				break;
			case 4:
				system("cls");
				file.open(GOODS_SHEET);
				deleteNote(file);
				file.close();
				break;
			case -1:
				system("cls");
				trigger = true;
				break;
			}
			system("pause");
		} while (trigger == false);
		
	}
	if (id == 2) {
		cout << "\n����� �������\n\n";
		bool trigger = false;
		int task{};
		do {
			cout << '\n';
			menuPurchase(task);
			switch (task)
			{
			case 1:
				system("cls");
				file.open(SALES_REPORT);
				readReport(file);
				file.close();
				break;
			case -1:
				system("cls");
				trigger = true;
				break;
			}
			system("pause");
		} while (trigger == false);
	}
}