#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include "FuncIni.h"
#include "Goods.h"

using namespace std;

void makeReport(fstream& file, Goods& obj, int count) {
	std::time_t t = time(0);
	std::tm* now = std::localtime(&t);
	float price = float(obj.getPrice().getRubs()) + float(obj.getPrice().getCops()) / 100;
	file << obj.getTitle() << " | "
		<< "�����: "
		<< now->tm_mday << '-'
		<< now->tm_mon + 1 << '-'
		<< now->tm_year + 1900 << "  "
		<< now->tm_hour << ':'
		<< now->tm_min << ':'
		<< now->tm_sec << " | ";
	if (obj.getQuantity() >= count) {
		file << "�������" << " | " << "����������: " << obj.getQuantity() - count << " | "
			<< "�������: " << obj.getQuantity() * price - count * price << '\n';
	}
	else {
		file << "�������" << " | " << "����������: " << count - obj.getQuantity() << " | "
			<< "�������: " << (count * price - obj.getQuantity() * price) * 0.7 << '\n';
	}
}
void makeReport(fstream& file, Goods& obj) {
	float price = float(obj.getPrice().getRubs()) + float(obj.getPrice().getCops()) / 100;
	std::time_t t = time(0);
	cout << t;
	std::tm* now = std::localtime(&t);
	file << obj.getTitle() << " | "
		<< "�����: "
		<< now->tm_mday << '-'
		<< now->tm_mon + 1 << '-'
		<< now->tm_year + 1900 << "  "
		<< now->tm_hour << ':'
		<< now->tm_min << ':'
		<< now->tm_sec << " | ";
	file << "�������" << " | " << "����������: " << obj.getQuantity() << " | "
		<< "�������: " << (obj.getQuantity() * price) * 0.7 << '\n';
}
void editNote(fstream& file) {
	Goods obj;
	Goods target;
	vector<Goods> array_goods;
	string buffer;
	bool fl{};
	int count{}, k{};
	cout << "������� �������� ������: ";
	cin.clear();
	cin >> buffer;
	while (!file.eof()) {
		file >> obj;
		if (obj.getTitle() == buffer) {
			fl = true;
			cout << "���������� ��������� ������: ";
			cin >> k;
			count = obj.getQuantity() - k;
			target = obj;
			if (count >= 0) {
				obj.setQuantity(count);
			}
			else {
				cout << "\n������: ����� ������ �����������\n";
			}

			//cin >> obj; //������ ��������������
		}
		array_goods.push_back(obj);
	}

	if (fl == false) {
		cout << "\n������ �� �������\n";
	}
	else {
		if (array_goods.empty()) {
			cout << "������ ���\n";
		}
		else {
			file.close();
			file.open(SALES_REPORT, std::ofstream::out | ios::app);
			makeReport(file, target, count);
			file.close();
			file.open(GOODS_SHEET, std::ofstream::out | std::ofstream::trunc);
			for (Goods it : array_goods) {
				printNote(file, it);
			}
		}
		cout << "\n������ ���������������\n";
	}
}

void deleteNote(fstream& file) {
	Goods obj;
	vector<Goods> array_goods;
	string buffer;
	bool fl{};
	cout << "������� �������� ������: ";
	cin.clear();
	cin >> buffer;

	while (!file.eof()) {
		file >> obj;
		if (obj.getTitle() != buffer) {
			array_goods.push_back(obj);
		}
		else {
			fl = true;
		}
	}
	if (fl == false) {
		cout << "\n������ �� �������\n";
	}
	else {
		file.close();
		file.open(GOODS_SHEET, std::ofstream::out | std::ofstream::trunc);
		if (array_goods.empty()) {
			cout << "������ ���\n";
		}
		else {
			for (Goods it : array_goods) {
				printNote(file, it);
			}
		}
		cout << "\n������ �������\n";
	}
}

void addNote(fstream& out) {
	Goods obj;
	cin >> obj;
	out.seekp(0, ios_base::end);
	printNote(out, obj);
	out.close();
	out.open(SALES_REPORT, ios::app);
	makeReport(out, obj);
	//out << '\n' << obj;
}
istream& operator>>(istream& in, Goods& obj) {
	Money* price = new Money;
	string buffer;
	unsigned long int value;
	cout << "������� �������� ������: ";
	in >> buffer;
	obj.setTitle(buffer);
	cout << "����������: ";
	in >> value;
	obj.setQuantity(value);
	cout << "���: ";
	in >> value;
	obj.setWeight(value);
	cout << "���� ��������: ";
	in >> value;
	obj.setShelfLife(value);
	cout << "����: \n";
	cout << "�����: ";
	in >> value;
	price->setRubs(value);
	cout << "�������: ";
	in >> value;
	price->setCops(value);
	obj.setPrice(*price);
	delete price;

	return in;
}

void readSheet(fstream& file) {
	Goods obj;
	vector<Goods> array_goods;
	if (file.peek() == EOF) {
		cout << "������ ���\n";
	}
	else {
		while (!file.eof()) {
			file >> obj;
			if (obj.getTitle() != "") {
				array_goods.push_back(obj);
			}
		}
		cout << "��������� �������:\n";
		for (int i{}; i < array_goods.size(); i++) {//���������� ��������� ��������
			cout << i + 1 << ". " << array_goods[i];
		}
	}
}

void readReport(fstream& file) {
	string buffer;
	int n{};
	while (!file.eof()) {
		getline(file, buffer);
		if (!buffer.empty()) {
			cout << ++n << ". " << buffer << '\n';
		}
	}
}

int menuSale(int& task)
{
	setlocale(LC_ALL, "Russian");
	task = 0;
	system("cls");
	cout << "�������� ������:\n";
	cout << "1-����������� �������� �������\n";
	cout << "2-�������� ������\n";
	cout << "3-�������� �������\n";
	cout << "4-������� ������\n";
	cout << "5-�����\n>> ";
	cin >> task;
	if ((task < 1) && (task > 5))
	{
		cout << "������������ ����\n\n";
		system("pause");
		menuSale(task);
	}
	if ((task >= 1) && (task <= 4))
	{
		return 1;
		system("cls");
	}
	if (task == 5)
	{
		task = -1;
	}
}

int menuPurchase(int& task)
{
	setlocale(LC_ALL, "Russian");
	task = 0;
	system("cls");
	cout << "�������� ������:\n";
	cout << "1-����������� �����\n";
	cout << "2-�����\n>> ";
	cin >> task;
	if ((task < 1) && (task > 2))
	{
		cout << "������������ ����\n\n";
		system("pause");
		menuSale(task);
	}
	if (task == 1)
	{
		return 1;
		system("cls");
	}
	if (task == 2)
	{
		task = -1;
	}
}

fstream& operator>>(fstream& file, Goods& obj) {
	int i{}, k{};
	file.clear();
	string buffer_string{};
	string buffer_word{};

	string title{};
	unsigned long int quantity{};
	unsigned long int weight{};
	unsigned long int shelfLife{};
	Money price{};

	getline(file, buffer_string);
	if (buffer_string.empty()) {
		return file;
	}
	while (!isalnum(buffer_string[i])) {
		i++;
	}
	//������ ���������
	while (buffer_string[i] != '|') {
		i++;
		k++;
	}

	title.assign(buffer_string, i - k, k - 1);
	k = 0;
	while (!isalnum(buffer_string[i])) {
		i++;
	}
	//������ ���-��
	while (buffer_string[i] != '|') {
		i++;
		k++;
	}
	buffer_word.assign(buffer_string, i - k, k - 1);
	quantity = charToInt(buffer_word.c_str(), buffer_word.size());
	k = 0;

	buffer_word.clear();
	while (!isalnum(buffer_string[i])) {
		i++;
	}
	//������ ����
	while (buffer_string[i] != '|') {
		i++;
		k++;
	}
	buffer_word.assign(buffer_string, i - k, k - 1);
	weight = charToInt(buffer_word.c_str(), buffer_word.size());
	k = 0;

	buffer_word.clear();
	while (!isalnum(buffer_string[i])) {
		i++;
	}
	//������ ����� ��������
	while (buffer_string[i] != '|') {
		i++;
		k++;
	}
	buffer_word.assign(buffer_string, i - k, k - 1);
	shelfLife = charToInt(buffer_word.c_str(), buffer_word.size());
	k = 0;

	buffer_word.clear();
	while (!isalnum(buffer_string[i])) {
		i++;
	}
	//������ ����
	while (isdigit(buffer_string[i])) {
		i++;
		k++;
	}
	buffer_word.assign(buffer_string, i - k, k);
	price.setRubs(charToInt(buffer_word.c_str(), buffer_word.size()));
	i++;
	k = 0;
	buffer_word.clear();
	while (isdigit(buffer_string[i])) {
		i++;
		k++;
	}
	buffer_word.assign(buffer_string, i - k, k);
	price.setCops(charToInt(buffer_word.c_str(), buffer_word.size()));
	obj.setTitle(title);
	obj.setQuantity(quantity);
	obj.setWeight(weight);
	obj.setShelfLife(shelfLife);
	obj.setPrice(price);

	return file;
}

unsigned long int charToInt(const char* x, int k) {
	unsigned long int number = 0;
	int digit = 1, deg = k - 1;
	for (int i = 0; i < k; i++) {
		if (isdigit(x[i])) {
			digit = pow(10, deg) * (int(x[i]) - 48);
			number += digit;
			deg--;
		}
	}
	if (int(x[0]) == 45) {
		number /= -10;
	}
	return number;
}