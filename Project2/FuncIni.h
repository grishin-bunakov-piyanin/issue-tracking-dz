#pragma once
#include <string>
#include <fstream>
#include "main.h"
#include "Goods.h"

std::fstream& operator>>(std::fstream& in, Goods& obj);
//std::ifstream& readGoodsSheet(std::ifstream& in, Goods& obj);
unsigned long int charToInt(const char* x, int k);
int menuSale(int& task);
void readSheet(std::fstream& in);
void addNote(std::fstream& file);
void editNote(std::fstream& file);
void deleteNote(std::fstream& file);
std::istream& operator>>(std::istream& in, Goods& obj);
void makeReport(std::fstream& file, Goods& obj, int count);
int menuPurchase(int& task);
void readReport(std::fstream& file);