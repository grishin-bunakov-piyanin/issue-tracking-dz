#include "Money.h"

using namespace std;

Money::Money() {
	_rubles = 0;
	_copecks = 0;
}
Money::Money(unsigned long int rub, unsigned long int cop) {
	while (cop > 99) {
		rub += 1;
		cop -= 100;
	}
	_rubles = rub;
	_copecks = cop;
}
Money::Money(const Money& sum) {
	_rubles = sum.getRubs();
	_copecks = sum.getCops();
}
const unsigned long int Money::getRubs() const {
	return _rubles;
}
const unsigned long int Money::getCops() const {
	return _copecks;
}
void Money::setRubs(const unsigned long int rub) {
	_rubles = rub;
}
void Money::setCops(unsigned long int cop) {
	while (cop > 99) {
		_rubles += 1;
		cop -= 100;
	}
	_copecks = cop;
}
Money& Money::operator=(const Money& obj) {
	this->setRubs(obj.getRubs());
	this->setCops(obj.getCops());
	return *this;
}

/*std::ofstream& operator<<(std::ofstream& out, const Money& obj) {
	out << obj.getRubs() << '.' << obj.getCops() << " ���.";
	return out;
}*/
std::ostream& operator<<(std::ostream& out, const Money& obj) {
	out << obj.getRubs() << '.' << obj.getCops();
	return out;
}