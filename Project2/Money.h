#pragma once
#include <fstream>
class Money {
	unsigned long int _rubles{};
	unsigned long int _copecks{};
public:
	Money();
	Money(unsigned long int rub, unsigned long int cop);
	Money(const Money& sum);
	const unsigned long int getRubs() const;
	const unsigned long int getCops() const;
	void setRubs(const unsigned long int rub);
	void setCops(unsigned long int cop);
	Money& operator=(const Money& obj);
};
//std::ofstream& operator<<(std::ofstream& out, const Money& obj);
std::ostream& operator<<(std::ostream& out, const Money& obj);