#include "Goods.h"
#include "Money.h"
using namespace std;

Goods::Goods() {
	_title = {};
	_quantity = 0;
	_weight = 0;
	_shelfLife = 0;
	_price.setCops(0);
	_price.setRubs(0);
}
Goods::Goods(const Goods& obj) {
	this->setTitle(obj.getTitle());
	this->setQuantity(obj.getQuantity());
	this->setWeight(obj.getWeight());
	this->setShelfLife(obj.getShelfLife());
	this->setPrice(obj.getPrice());
}
const string Goods::getTitle() const {
	return _title;
}
const unsigned long int Goods::getQuantity() const {
	return _quantity;
}
const unsigned long int Goods::getWeight() const {
	return _weight;
}
const unsigned long int Goods::getShelfLife() const {
	return _shelfLife;
}
const Money Goods::getPrice() const {
	return _price;
}
void Goods::setTitle(string str) {
	_title = str;
}
void Goods::setQuantity(const unsigned long int value) {
	_quantity = value;
}
void Goods::setWeight(const unsigned long int value) {
	_weight = value;
}
void Goods::setShelfLife(const unsigned long int value) {
	_shelfLife = value;
}
void Goods::setPrice(const Money& price) {
	_price = price;
}

std::fstream& printNote(std::fstream& out, Goods& obj) {
	if (out.tellp() != 0) {
		out << '\n';
	}
	out << obj.getTitle() << " | "
		<< obj.getQuantity() << " | "
		<< obj.getWeight() << " | "
		<< obj.getShelfLife() << " | "
		<< obj.getPrice();
	return out;
}

std::ostream& operator<<(std::ostream& out, const Goods& obj) {
	out << obj.getTitle() << " | "
		<< obj.getQuantity() << " ��." << " | "
		<< obj.getWeight() << " �" << " | "
		<< obj.getShelfLife() << " ����" << " | "
		<< obj.getPrice() << " ���."
		<< '\n';
	return out;
}